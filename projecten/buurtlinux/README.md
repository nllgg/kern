# BuurtLinux

Het gebruik van software gaat niet altijd over rozen. Met open source software, en dus Linux, is dat niet anders. Dan is het fijn om iemand in de buurt te (leren) kennen, die met jou mee kan denken. Hoe handig is het dan als je op een kaart kan kijken, om te zien wie zich daar voor leent?

# Discussie

De discussie :speech_balloon: vind plaats in #6
