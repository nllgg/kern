# Kern

Project: de vereniging Nederlandse Linux Gebruikers Groep. We gaan de repo
gebruiken als veredele takenlijst en vervanging van de wiki. Bijdragen zijn
welkom!

# Ledenadministratie

De ledenadministratie is omgezet naar [CiviCRM](https://civicrm.org). Daarmee is
het nog niet klaar. Daarom hebben we de ledenadministratie een
[eigen project](https://gitlab.com/nllgg/civicrm) gegeven. 