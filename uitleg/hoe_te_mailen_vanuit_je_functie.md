# Hoe te mailen vanuit je functie

Binnen het bestuur en een commissie (zoals de programmacommissie) vervul je een taak. Bij de meeste functies (zoals voorzitter, penningmeester en secretaris) hoort een e-mailadres. Zo kunnen mensen niet naar een persoon, maar naar een functie mailen. 

Momenteel staat hieronder alleen instructies voor onze webmail. Aanvullingen voor andere software zijn van harte welkom. Let daarbij op dat mail ook via onze server uitgezonden moet worden anders wordt de mail op goed geconfigureerde mailservers, geweigerd omdat alleen onze mailserver in het [SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework) record staan.


## Voorbeeld: mail van de secretaris
Binnen onze mailomgeving is er een map `secretaris`. Alle mail voor _secretaris@nllgg.nl_ komt daarin.
Om zaken voor de ontvangers duidelijk te houden, is het daarom belangrijk dat mail ook verzonden wordt met _secretaris@nllgg.nl_ als afzender.
Ook is het belangrijk dat wanneer een ontvanger reageert op een mail, afkomstig van secretaris, deze reactie weer aan _secretaris@nllgg.nl_ wordt verzonden.
Ten slotte willen we graag een overzicht behouden van wat er vanuit verschillende functies verstuurd wordt. Daarom voegen we hieronder steeds ook een **BCC** naar onze eigen functie in. Dat is met name belangrijk wanneer meerdere mensen gezamenlijk een functie bekleden, zoals bijvoorbeeld in de programmacommissie. Ieder lid dat in de `progcie` map kan kijken ziet meteen dat een mail beantwoord is.

# Webmail

Deze manual legt uit hoe je onze [webmail](https://webmail.nllgg.nl) (Roundcube) zo kunt instellen dat dit goed werkt.

## Een signature instellen

- Ga naar het instellingen scherm `Identities`.
  - _In de mailsoftware is een knop `Settings`, rechtsboven met een tandwieltje er naast. Klik daar op._
  - _Klik in het menu, dat nu aan de linkerkant verschijnt, op `Identities`._
- Klik onderaan op de `+` knop.
- Voeg een identity toe met de volgende eigenschappen onder **Settings**:

  |Setting|Value|Value|
  |---|---|---|
  |Display Name|Secretaris NLLGG|Programmacommissie NLLGG|
  |Email|`secretaris@nllgg.nl`|`progcie@nllgg.nl`|
  |Company|Nederlandse Linux Gebruikers Groep|Nederlandse Linux Gebruikers Groep|
  |Reply-to|`secretaris@nllgg.nl`|`progcie@nllgg.nl`|
  |Bcc|`secretaris@nllgg.nl`|`progcie@nllgg.nl`|

  Als je ook mails op persoonlijke titel moet kunnen versturen stel je `Set default` het beste niet in.

- Voeg vervolgens deze **Signature** toe:
  ```
  Met vriendelijke groet,

  JOUW EIGEN NAAM, FUNCTIE Nederlandse Linux Gebruikers Groep
  https://nllgg.nl/
  https://gitlab.com/nllgg/
  https://www.meetup.com/Nederlandse-Linux-Gebruikers-Groep-Bijeenkomsten/
  https://twitter.com/NLLGG
  https://facebook.com/NLLGG
  m: 06-JOUW EIGEN TELEFOONNUMMER
  Join ons op IRC, irc.oftc.net/#nllgg
  ```
  _Let op dat je je naam en telefoonnummer vervangt._
  Omdat we nog een flink aantal leden met mailsoftware zonder HTML ondersteuning hebben, maken we hier, op dit moment, geen `HTML signature` van.

- Herhaal dit process voor je persoonlijke mail, maar dan zonder dat `Reply-To` adres en `BCC` adres.
- Wanneer je meerdere functies bekleed herhaal je dit voor elke afzonderlijke functie.

Wanneer je nu een nieuwe mail gaat samenstellen kun je bovenaan kiezen uit verschillende `From` keuzes.
